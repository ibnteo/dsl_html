<?php
class HTML {
	static $path = [];
	static function __callStatic($name, $args) {
		$attr = '';
		if (sizeof($args) > 0 && is_array($args[0])) {
			foreach ($args[0] as $n=>$v) {
				$n = htmlspecialchars($n);
				if ($v !== false) {
					if (is_array($v)) {
						$param = [];
						foreach ($v as $key=>$val) {
							$key = htmlspecialchars($key);
							if (is_array($val)) $val = implode(',', $val);
							$val = htmlspecialchars($val);
							if ($n == 'transform') $param[] = "$key($val)";
							elseif ($n == 'style') $param[] = "$key:$val;";
							elseif ($n == 'content') $param[] = "$key=$val";
							elseif (is_numeric($key)) $param[] = "$val";
							else {$sep = ($key ? '-' : ''); $attr .= " $n$sep$key=\"$val\"";}
						}
						if (sizeof($param) > 0) $v = implode(' ', $param);
					} elseif ($name === 'path' && $n === 'd' && $v instanceof Closure) {
						$d = new SVG();
						$v($d);
						$v = htmlspecialchars($d->d);
					} else if ($v !== true) {
						$v = htmlspecialchars($v);
					}
					if ($v === true) {
						$attr .= " $n";
					} elseif (! is_array($v)) {
						$attr .= " $n=\"$v\"";
					}
				}
			}
			if ($name === 'svg' && ! isset($args[0]['xmlns'])) {
				$attr .= ' xmlns="'.SVG::xmlns.'"';
			}
		} else {
			if ($name === 'svg') {
				$attr = ' xmlns="'.SVG::xmlns.'"';
			}
		}
		if ($name === 'doctype') {
			$doctype = $args[0] ?? 'html';
			echo "<!$name $doctype>\n";
		} elseif ($name === 't' && sizeof($args) == 1) {
			echo htmlspecialchars($args[0]);
		} elseif ($name == 'e' || $name == 'end') {
			$tag = array_pop(HTML::$path);
			if ($tag) echo "</$tag>" . ((HTML::$path[sizeof(HTML::$path) - 1] ?? 'a') === 'a' ? '' : "\n");
		} elseif ($name == 'block') {
			array_push(HTML::$path, '');
		} elseif ($name == 'echo') {
			echo implode("\n", $args);
		} elseif ($name == 'style') {
			$style = $args[1] ?? (! is_array($args[0]) ? ($args[0] ?? '') : '');
			echo "<$name$attr>$style</$name>\n";
		} elseif ($name == 'script') {
			$script = $args[1] ?? (! is_array($args[0]) ? ($args[0] ?? '') : '');
			echo "<$name$attr>$script</$name>\n";
		} elseif (in_array($name, ['br','hr','col','meta','link','img','input','source','circle','ellipse','rect','line','path','polygon','polyline'])) {
			echo "<$name$attr/>\n";
		} elseif (sizeof($args) > 0 && ! is_array($args[0])) {
			$text = htmlspecialchars($args[0]);
			echo "<$name$attr>$text</$name>\n";
		} elseif (sizeof($args) > 1) {
			if ($args[1] === true) {
				echo "<$name$attr/>\n";
			} else {
				$text = htmlspecialchars($args[1]);
				echo "<$name$attr>$text</$name>\n";
			}
		} else {
			array_push(HTML::$path, $name);
			echo "<$name$attr>\n";
		}
	}
	static function js($a, $r=false) {
		$js = ($r?'':"\n");
		foreach ($a as $n=>$v) {
			if (is_array($v)) {
				$js .= "$n { ";
				foreach ($v as $key=>$val) {
					if (is_array($val)) {
						$js .= HTML::js($val, true);
					} else {
						$js .= "$val; ";
					}
				}
				$js .= "}".($r?' ':"\n");
			} else {
				$js .= "$v; ";
			}
		}
		return $js;
	}
	static function css($a) {
		$css = "\n";
		foreach ($a as $n=>$v) {
			if (is_array($v)) {
				$css .= "$n {";
				foreach ($v as $key=>$val) {
					if (is_array($val)) {
						$css .= "$key {";
						foreach ($val as $key2=>$val2) {
							if ($val2 && is_int($val2)) $val2 .= 'px';
							$css .= "$key2:$val2;";
						}
						$css .= "}";
					} else {
						if ($val && is_int($val)) $val .= 'px';
						$css .= "$key:$val;";
					}
				}
				$css .= "}\n";
			} else {
				$css .= "$n $v;\n";
			}
		}
		return $css;
	}
	static function stylesheet($src) {
		return HTML::link(['rel'=>'stylesheet','href'=>$src]);
	}
	static function javascript($src) {
		return HTML::script(['type'=>'text/javascript', 'src'=>$src]);
	}
}
class SVG {
	const xmlns = 'http://www.w3.org/2000/svg';
	public $d = '';
	function __call($n, $a) {
		if ($n === 'Move') $this->d .= 'M'.$a[0].' '.$a[1];
		elseif ($n === 'move') $this->d .= 'm'.$a[0].' '.$a[1];
		elseif ($n === 'Line') $this->d .= 'L'.$a[0].' '.$a[1];
		elseif ($n === 'line') $this->d .= 'l'.$a[0].' '.$a[1];
		elseif ($n === 'Horiz') $this->d .= 'H'.$a[0];
		elseif ($n === 'horiz') $this->d .= 'h'.$a[0];
		elseif ($n === 'Vert') $this->d .= 'V'.$a[0];
		elseif ($n === 'vert') $this->d .= 'v'.$a[0];
		elseif ($n === 'Zero') $this->d .= 'Z';
		elseif ($n === 'zero') $this->d .= 'z';
		elseif ($n === 'Rect') $this->d .= 'M'.$a[0].' '.$a[1].'h'.$a[2].'v'.$a[3].'h'.(-$a[2]).'z';
		elseif ($n === 'rect') $this->d .= 'm'.$a[0].' '.$a[1].'h'.$a[2].'v'.$a[3].'h'.(-$a[2]).'z';
		elseif ($n === 'Arc') $this->d .= 'A'.$a[0].' '.$a[1].' '.$a[2].' '.$a[3].' '.$a[4].' '.$a[5].' '.$a[6];
		elseif ($n === 'arc') $this->d .= 'a'.$a[0].' '.$a[1].' '.$a[2].' '.$a[3].' '.$a[4].' '.$a[5].' '.$a[6];
		elseif ($n === 'Ellipse') $this->d .= 'M'.($a[0]-$a[2]).' '.$a[1].'a'.$a[2].' '.$a[3].' 0 1 0 '.($a[2]*2).' 0a'.$a[2].' '.$a[3].' 0 1 0 '.(-$a[2]*2).' 0';
		elseif ($n === 'ellipse') $this->d .= 'm'.($a[0]-$a[2]).' '.$a[1].'a'.$a[2].' '.$a[3].' 0 1 0 '.($a[2]*2).' 0a'.$a[2].' '.$a[3].' 0 1 0 '.(-$a[2]*2).' 0';
		elseif ($n === 'Circle') $this->Ellipse($a[0], $a[1], $a[2], $a[2]);
		elseif ($n === 'circle') $this->ellipse($a[0], $a[1], $a[2], $a[2]);
		elseif ($n === 'Cubic') $this->d .= 'C'.$a[0].' '.$a[1].' '.$a[2].' '.$a[3].' '.$a[4].' '.$a[5];
		elseif ($n === 'cubic') $this->d .= 'c'.$a[0].' '.$a[1].' '.$a[2].' '.$a[3].' '.$a[4].' '.$a[5];
		elseif ($n === 'SCubic') $this->d .= 'S'.$a[0].' '.$a[1].' '.$a[2].' '.$a[3];
		elseif ($n === 'sCubic') $this->d .= 's'.$a[0].' '.$a[1].' '.$a[2].' '.$a[3];
		elseif ($n === 'Quadratic') $this->d .= 'Q'.$a[0].' '.$a[1].' '.$a[2].' '.$a[3];
		elseif ($n === 'quadratic') $this->d .= 'q'.$a[0].' '.$a[1].' '.$a[2].' '.$a[3];
		elseif ($n === 'SQuadratic') $this->d .= 'T'.$a[0].' '.$a[1];
		elseif ($n === 'sQuadratic') $this->d .= 't'.$a[0].' '.$a[1];
		return $this;
	}
	function __get($n) {
		return $this->$n();
	}
	function polyline($a) {
		if (! is_array($a)) {
			$points = [];
			foreach (explode(' ', $a['points']) as $point) {
				$points[] = [explode(',', $point)];
			}
		} else {
			$points = $a;
		}
		foreach ($points as $i=>$point) {
			if ($i == 0) $this->Move($point[0], $point[1]); else $this->Line($point[0], $point[1]);
		}
		return $this;
	}
	function polygon($a) {
		$this->polyline($a);
		$this->zero();
		return $this;
	}
}
function e(...$args) {return HTML::e(...$args);}
function doctype(...$args) {return HTML::doctype(...$args);}
function html(...$args) {return HTML::html(...$args);}
function stylesheet(...$args) {return HTML::stylesheet(...$args);}
function javascript(...$args) {return HTML::javascript(...$args);}

function svg(...$args) {return HTML::svg(...$args);}
function circle(...$args) {return HTML::circle(...$args);}
function ellipse(...$args) {return HTML::ellipse(...$args);}
function g(...$args) {return HTML::g(...$args);}
function image(...$args) {return HTML::image(...$args);}
function line(...$args) {return HTML::line(...$args);}
function linearGradient(...$args) {return HTML::linearGradient(...$args);}
function marker(...$args) {return HTML::marker(...$args);}
function mask(...$args) {return HTML::mask(...$args);}
function path(...$args) {return HTML::path(...$args);}
function polygon(...$args) {return HTML::polygon(...$args);}
function polyline(...$args) {return HTML::polyline(...$args);}
function rect(...$args) {return HTML::rect(...$args);}
function symbol(...$args) {return HTML::symbol(...$args);}
function text(...$args) {return HTML::text(...$args);}
function textPath(...$args) {return HTML::textPath(...$args);}
function tref(...$args) {return HTML::tref(...$args);}
function tspan(...$args) {return HTML::tspan(...$args);}
function defs(...$args) {return HTML::defs(...$args);}

function a(...$args) {return HTML::a(...$args);}
function abbr(...$args) {return HTML::abbr(...$args);}
function address(...$args) {return HTML::address(...$args);}
function area(...$args) {return HTML::area(...$args);}
function article(...$args) {return HTML::article(...$args);}
function aside(...$args) {return HTML::aside(...$args);}
function audio(...$args) {return HTML::audio(...$args);}
function base(...$args) {return HTML::base(...$args);}
function b(...$args) {return HTML::b(...$args);}
function bdi(...$args) {return HTML::bdi(...$args);}
function bdo(...$args) {return HTML::bdo(...$args);}
function blockquote(...$args) {return HTML::blockquote(...$args);}
function body(...$args) {return HTML::body(...$args);}
function br(...$args) {return HTML::br(...$args);}
function button(...$args) {return HTML::button(...$args);}
function canvas(...$args) {return HTML::canvas(...$args);}
function caption(...$args) {return HTML::caption(...$args);}
function cite(...$args) {return HTML::cite(...$args);}
function code(...$args) {return HTML::code(...$args);}
function col(...$args) {return HTML::col(...$args);}
function colgroup(...$args) {return HTML::colgroup(...$args);}
function data(...$args) {return HTML::data(...$args);}
function datalist(...$args) {return HTML::datalist(...$args);}
function dd(...$args) {return HTML::dd(...$args);}
function del(...$args) {return HTML::del(...$args);}
function details(...$args) {return HTML::details(...$args);}
function dfn(...$args) {return HTML::dfn(...$args);}
function dialog(...$args) {return HTML::dialog(...$args);}
function div(...$args) {return HTML::div(...$args);}
function dlist(...$args) {return HTML::dl(...$args);}
function dt(...$args) {return HTML::dt(...$args);}
function embed(...$args) {return HTML::embed(...$args);}
function fieldset(...$args) {return HTML::fieldset(...$args);}
function figcaption(...$args) {return HTML::figcaption(...$args);}
function figure(...$args) {return HTML::figure(...$args);}
function footer(...$args) {return HTML::footer(...$args);}
function form(...$args) {return HTML::form(...$args);}
function h1(...$args) {return HTML::h1(...$args);}
function h2(...$args) {return HTML::h2(...$args);}
function h3(...$args) {return HTML::h3(...$args);}
function h4(...$args) {return HTML::h4(...$args);}
function h5(...$args) {return HTML::h5(...$args);}
function h6(...$args) {return HTML::h6(...$args);}
function head(...$args) {return HTML::head(...$args);}
function hheader(...$args) {return HTML::header(...$args);}
function hr(...$args) {return HTML::hr(...$args);}
function html5(...$args) {return HTML::html(...$args);}
function i(...$args) {return HTML::i(...$args);}
function em(...$args) {return HTML::em(...$args);}
function iframe(...$args) {return HTML::iframe(...$args);}
function img(...$args) {return HTML::img(...$args);}
function input(...$args) {return HTML::input(...$args);}
function ins(...$args) {return HTML::ins(...$args);}
function kbd(...$args) {return HTML::kbd(...$args);}
function label(...$args) {return HTML::label(...$args);}
function legend(...$args) {return HTML::legend(...$args);}
function li(...$args) {return HTML::li(...$args);}
function hlink(...$args) {return HTML::link(...$args);}
function main(...$args) {return HTML::main(...$args);}
function map(...$args) {return HTML::map(...$args);}
function mark(...$args) {return HTML::mark(...$args);}
function meta(...$args) {return HTML::meta(...$args);}
function meter(...$args) {return HTML::meter(...$args);}
function nav(...$args) {return HTML::nav(...$args);}
function noscript(...$args) {return HTML::noscript(...$args);}
function obj(...$args) {return HTML::object(...$args);}
function ol(...$args) {return HTML::ol(...$args);}
function optgroup(...$args) {return HTML::optgroup(...$args);}
function option(...$args) {return HTML::option(...$args);}
function output(...$args) {return HTML::output(...$args);}
function param(...$args) {return HTML::param(...$args);}
function picture(...$args) {return HTML::picture(...$args);}
function p(...$args) {return HTML::p(...$args);}
function pre(...$args) {return HTML::pre(...$args);}
function progress(...$args) {return HTML::progress(...$args);}
function q(...$args) {return HTML::q(...$args);}
function ruby(...$args) {return HTML::ruby(...$args);}
function rb(...$args) {return HTML::rb(...$args);}
function rt(...$args) {return HTML::rt(...$args);}
function rtc(...$args) {return HTML::rtc(...$args);}
function rp(...$args) {return HTML::rp(...$args);}
function samp(...$args) {return HTML::samp(...$args);}
function script(...$args) {return HTML::script(...$args);}
function section(...$args) {return HTML::section(...$args);}
function select(...$args) {return HTML::select(...$args);}
function small(...$args) {return HTML::small(...$args);}
function source(...$args) {return HTML::source(...$args);}
function span(...$args) {return HTML::span(...$args);}
function strong(...$args) {return HTML::strong(...$args);}
function style(...$args) {return HTML::style(...$args);}
function sub(...$args) {return HTML::sub(...$args);}
function summary(...$args) {return HTML::summary(...$args);}
function sup(...$args) {return HTML::sup(...$args);}
function table(...$args) {return HTML::table(...$args);}
function tbody(...$args) {return HTML::tbody(...$args);}
function td(...$args) {return HTML::td(...$args);}
function template(...$args) {return HTML::template(...$args);}
function textarea(...$args) {return HTML::textarea(...$args);}
function tfoot(...$args) {return HTML::tfoot(...$args);}
function th(...$args) {return HTML::th(...$args);}
function thead(...$args) {return HTML::thead(...$args);}
function title(...$args) {return HTML::title(...$args);}
function tr(...$args) {return HTML::tr(...$args);}
function track(...$args) {return HTML::track(...$args);}
function u(...$args) {return HTML::u(...$args);}
function ul(...$args) {return HTML::ul(...$args);}
function video(...$args) {return HTML::video(...$args);}
function wbr(...$args) {return HTML::wbr(...$args);}
function noindex(...$args) {return HTML::noindex(...$args);}
