var doc = document;
function getid(i) {return doc.getElementById(i);}
function all(s, n) {if (! n) n = doc; return n.querySelectorAll(s);}
function one(s, n) {if (! n) n = doc; return n.querySelector(s);}
function load(f) {window.addEventListener("load", f);}
function pause(s, f) {return window.setTimeout(f, s * 1000);}
function interval(s, f) {return window.setInterval(f, s * 1000);}
function kebab(s) {return s.replace(/_[A-Z]/g, function(m) {return "-" + m.substr(1).toLowerCase()});}
function node(o) {return typeof(o) == "string" ? one(o) : o;}
function each() {
	if (arguments.length == 2 && typeof(arguments[0]) == "object") {
		var obj = arguments[0];
		var f = arguments[1];
        for (var key in obj) {
            if (obj.constructor != NodeList || ! isNaN(parseInt(key))) f(obj[key], key);
        }
	} else if (arguments.length > 2 || (arguments.length == 2 && typeof(arguments[0]) == "number")) {
		var start = 0;
		var end = 0;
		var step = 1;
		var f = arguments[arguments.length - 1];
		if (arguments.length == 2) {
			end = arguments[0];
		} else if (arguments.length == 3) {
			start = arguments[0];
			end = arguments[1];
		} else {
			start = arguments[0];
			end = arguments[1];
			step = arguments[2];
		}
		if (step > 0 && start < end) for (var index = start; index < end; index += step) f(index);
		else if (step < 0 && start > end) for (var index = start; index > end; index += step) f(index);
	} else if (arguments.length == 2) {
		var obj = all(arguments[0]);
		var f = arguments[1];
		for (var val in obj) f(val);
	}
}
function show(el) {
	el.style.display = "block";
}
function hide(el) {
	el.style.display = "none";
}
function toggle(el, sw, t) {
    if (! sw) sw = false;
    if (! t) t = "opacity";
    if (el.style.display == "none") show(el, t); else hide(el, sw ? null : t);
}
var cursor = {type:"append", node:null, xmlns:null};
function insert(n) {
	if (! cursor.node) cursor.node = doc.body;
	if (cursor.type == "append") {
		cursor.node.appendChild(n);
	} else if (cursor.type == "prepend") {
		cursor.node.prependChild(n);
		cursor.type = "after";
		cursor.node = n;
	} else if (cursor.type == "replace") {
		cursor.node.parentNode.replaceChild(n, cursor.node);
		cursor.type = "after";
		cursor.node = n;
	} else if (cursor.type == "after") {
		cursor.node.parentNode.insertBefore(n, cursor.node.nextSibling);
		cursor.node = n;
	} else if (cursor.type == "before") {
		cursor.node.parentNode.insertBefore(n, cursor.node);
		cursor.node = n;
	}
	return n;
}
each(["append","prepend","replace","before","after"], function(t) {window[t] = function(n, f, xmlns) {
	var cur = cursor;
	cursor = {node:node(n), type:t, xmlns:xmlns};
	f(cursor.node);
	cursor = cur;
	return n;
}});
function text(t, f) {
	if (typeof(t) == "object") {
		return tag("text", t, f);
	} else {
		return insert(doc.createTextNode(t));
	}
}
function tag(t, a, f, xmlns) {
	if (f === undefined && typeof(a) != "object") {
		f = a;
		a = {};
	}
	var el = (xmlns ? xmlns : cursor.xmlns) ? doc.createElementNS(xmlns ? xmlns : cursor.xmlns, t) : doc.createElement(t);
	each(a, function(val, n) {
		if (typeof(val) === "object" && ! Array.isArray(val)) {
			var param = [];
			each(val, function(value, name) {
				if (n == "transform") param.push(kebab(name)+"("+value+")");
				else if (n == "style") param.push(kebab(name)+":"+value+";");
				else el.setAttribute(n + (name === "" || name === "_" ? "" : "-" + kebab(name)), value);
			});
			if (param.length > 0) val = param.join(" ");
		}
		if (Array.isArray(val)) {
			el.setAttribute(kebab(n), val.join(" "));
		} else if (val === false) {
		} else if (n == "class") {
			each(a.class.split(" "), function(n) {if (n) el.classList.add(n);});
		} else if (n.indexOf("on") == 0) {
			el.addEventListener(n.substr(2), val, false);
		} else if (typeof(val) !== "object") {
			el.setAttribute(kebab(n), val);
		}
	});
	if (typeof(f) == "function") append(el, function() {f(el);}, xmlns);
	else if (f !== undefined) append(el, function() {text(f);}, xmlns);
	insert(el);
	return el;
}
each(["a","em","strong","small","s","cite","q","dfn","abbr","data","time","code","var","samp","kbd","sub","sup","i","b","u","mark","ruby","rt","rp","bdi","bdo","span","table","caption","colgroup","tbody","thead","tfoot","tr","td","th","body","section","nav","article","aside","h1","h2","h3","h4","h5","h6","header","footer","address","main","script","noscript","head","title","style","details","summary","menu","p","pre","blockquote","ol","ul","li","dl","dt","dd","figure","figcaption","div","form","fieldset","legend","label","button","select","datalist","optgroup","option","textarea","output","progress","meter","iframe","object","video","audio","canvas","map","ins","del"], function(t) {window[t] = function(a, f) {return tag(t, a, f);};});
each(["area","base","basefont","bgsound","br","col","command","embed","hr","img","input","isindex","keygen","link","meta","param","source","track","wbr"], function(t) {window[t] = function(a) {return tag(t, a);};});
