<?php
require ('dslHtml.php');
doctype();
html(['lang'=>'en']);
    head();
        meta(['charset'=>'utf-8']);
        title('DSL HTML JS/PHP');
        javascript('dslHtml.js');
        javascript('dslSvg.js');
    e('head');
    body();
        div(['class'=>['text-center','p-5'], 'style'=>['background'=>'#ddd']]);
            text('Text ');
            b('Bold');
            text(' ');
            i('Italic');
        e('div');
        svg(['width'=>100, 'height'=>100, 'viewBox'=>[0,0,100,100]]);
            path(['stroke'=>'black', 'stroke-width'=>2, 'd'=>function($d) {
                $d->Move(0,0)->line(100,100);
            }]);
        e('svg');
        $js = [
            'function test(txt)'=>[
                'div({class:["m-5"], style:{color:"blue"}}, txt)',
                'div({class:["m-5"], style:{color:"red"}}, function() {text("Test 2");})',
                'div({class:["d-flex"], style:{background:"#ddd"}}, function() {text("Text "); b("Bold"); text(" "); i("Italic");})',
            ],
            'test("Test 1")',
            'svg({width:100, height:100, viewBox:[0,0,100,100]}, function() {path({stroke:"black", stroke_Width:2}, function(d) {d.Move(0,0).line(100,100);});})',
        ];
        script(['type'=>'text/javascript'], HTML::js($js));
    e('body');
e('html');