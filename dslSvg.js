var svgXmlns = "http://www.w3.org/2000/svg";
each(["animate","animateMotion","animateTransform","circle","clipPath","defs","desc","discard","ellipse","feBlend","feColorMatrix","feComponentTransfer","feComposite","feConvolveMatrix","feDiffuseLighting","feDisplacementMap","feDistantLight","feDropShadow","feFlood","feFuncA","feFuncB","feFuncG","feFuncR","feGaussianBlur","feImage","feMerge","feMergeNode","feMorphology","feOffset","fePointLight","feSpecularLighting","feSpotLight","feTile","feTurbulence","filter","foreignObject","g","hatch","hatchpath","image","line","linearGradient","marker","mask","mesh","meshgradient","meshpatch","meshrow","metadata","mpath","path","pattern","polygon","polyline","radialGradient","rect","set","stop","switch","symbol","textPath","tspan","unknown","use","view"], function(t) {window[t] = function(a, f) {return tag(t, a, f, svgXmlns);}});
function svg(a, f) {
	if (f === undefined && typeof(a) != "object") {
		f = a;
		a = {};
	}
	a.xmlns = svgXmlns;
	return tag("svg", a, f, svgXmlns);
}
function pathPolyline(a) {
	var points = a.points;
	if (typeof(poinst) == "string") {
		points = [];
		each(a.points.split(" "), function(point) {points.push([point.split(",")]);});
	}
	delete a.points;
	return path(a, function(d) {
		each(points, function(point, i) {if (i == 0) d.Move(point[0], point[1]); else d.Line(point[0], point[1]);});
	});
}
function pathPolygon(a) {
	var points = a.points;
	if (typeof(poinst) == "string") {
		points = [];
		each(a.points.split(" "), function(point) {points.push([point.split(",")]);});
	}
	delete a.points;
	return path(a, function(d) {
		each(points, function(point, i) {if (i == 0) d.Move(point[0], point[1]); else d.Line(point[0], point[1]);});
		d.zero();
	});
}
function pathRect(a) {
	var x = a.x; var y = a.y; var width = a.width; var height = a.height;
	delete a.x; delete a.y; delete a.width; delete a.height;
	return path(a, function(d) {
		d.Move(x, y).horiz(width).vert(height).horiz(-width).zero();
	});
}
function pathCircle(a) {
	var cx = a.cx; var cy = a.cy; var r = a.r;
	delete a.cx; delete a.cy; delete a.r;
	return path(a, function(d) {
		d.Circle(cx, cy, r);
	});
}
function pathEllipse(a) {
	var cx = a.cx; var cy = a.cy; var rx = a.rx;; var ry = a.ry;
	delete a.cx; delete a.cy; delete a.rx; delete a.ry;
	return path(a, function(d) {
		d.Ellipse(cx, cy, rx, ry);
	});
}
function pathLine(a) {
	var x1 = a.x1; var y1 = a.y1; var x2 = a.x2; var y2 = a.y2;
	delete a.x1; delete a.y1; delete a.x2; delete a.y2;
	return path(a, function(d) {
		d.Move(x1, y1);
		d.Line(x2, y2);
	});
}
SVGPathElement.prototype.d = function (value) {
	this.setAttribute("d", (this.getAttribute("d") ? this.getAttribute("d") : "") + value);
}
var pathElement = SVGPathElement.prototype;
pathElement.Move = function (x, y) {this.d("M "+x+" "+y); return this;}
pathElement.move = function (dx, dy) {this.d("m "+dx+" "+dy); return this;}
pathElement.Line = function (x, y) {this.d("L "+x+" "+y); return this;}
pathElement.line = function (dx, dy) {this.d("l "+dx+" "+dy); return this;}
pathElement.Horiz = function (x) {this.d("H "+x); return this;}
pathElement.horiz = function (dx) {this.d("h "+dx); return this;}
pathElement.Vert = function (y) {this.d("V "+y); return this;}
pathElement.vert = function (dy) {this.d("v "+dy); return this;}
pathElement.Zero = function () {this.d("Z"); return this;}
pathElement.zero = function () {this.d("z"); return this;}
pathElement.Rect = function (x, y, width, height) {this.d("M "+x+" "+y+"h "+width+" v"+height+" h"+(-width)+" z"); return this;}
pathElement.rect = function (dx, dy, width, height) {this.d("m "+dx+" "+dy+"h "+width+" v"+height+" h"+(-width)+" z"); return this;}
pathElement.Arc = function (rx, ry, xAxisRotation, largeArcFlag, sweepFlag, x, y) {this.d("A "+rx+" "+ry+" "+xAxisRotation+" "+largeArcFlag+" "+sweepFlag+" "+x+" "+y); return this;}
pathElement.arc = function (rx, ry, xAxisRotation, largeArcFlag, sweepFlag, dx, dy) {this.d("a "+rx+" "+ry+" "+xAxisRotation+" "+largeArcFlag+" "+sweepFlag+" "+dx+" "+dy); return this;}
pathElement.Circle = function (cx, cy, r) {return this.Ellipse(cx, cy, r, r);}
pathElement.circle = function (cx, cy, r) {return this.ellipse(cx, cy, r, r);}
pathElement.Ellipse = function (cx, cy, rx, ry) {this.d("M "+(cx-rx)+" "+cy+"a "+rx+" "+ry+" 0 1 0 "+(rx*2)+" 0"+"a "+rx+" "+ry+" 0 1 0 "+(-rx*2)+" 0"); return this;}
pathElement.ellipse = function (cx, cy, rx, ry) {this.d("m "+(cx-rx)+" "+cy+"a "+rx+" "+ry+" 0 1 0 "+(rx*2)+" 0"+"a "+rx+" "+ry+" 0 1 0 "+(-rx*2)+" 0"); return this;}
pathElement.Cubic = function (x1, y1, x2, y2, x, y) {this.d("C "+x1+" "+y1+","+x2+" "+y2+","+x+" "+y); return this;}
pathElement.cubic = function (dx1, dy1, dx2, dy2, dx, dy) {this.d("c "+dx1+" "+dy1+","+dx2+" "+dy2+","+dx+" "+dy); return this;}
pathElement.Several = function (x2, y2, x, y) {this.d("S "+x2+" "+y2+","+x+" "+y); return this;}
pathElement.several = function (dx2, dy2, dx, dy) {this.d("s "+dx2+" "+dy2+","+dx+" "+dy); return this;}
pathElement.Quadratic = function (x1, y1, x, y) {this.d("Q "+x1+" "+y1+","+x+" "+y); return this;}
pathElement.quadratic = function (dx1, dy1, dx, dy) {this.d("q "+dx1+" "+dy1+","+dx+" "+dy); return this;}
pathElement.QuadraTic = function (x, y) {this.d("T "+x+" "+y); return this;}
pathElement.quadraTic = function (dx, dy) {this.d("t "+dx+" "+dy); return this;}
